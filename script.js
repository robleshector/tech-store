// Constructors
class Product {
  constructor(
    _id,
    name,
    price,
    stock,
    max,
    category,
    shippingCost,
    reviews = [],
    description,
    image
  ) {
    this._id = _id;
    this.name = name;
    this.price = price;
    this.stock = stock;
    this.max = max;
    this.category = category;
    this.shippingCost = shippingCost;
    this.reviews = reviews;
    this.description = description;
    this.image = image;
  }
}

function CartItem(productId, price, quantity, shippingCost) {
  this.productId = productId;
  this.price = price;
  this.quantity = quantity;
  this.shippingCost = shippingCost;
}

function ProductReview(review, rating) {
  this.review = review;
  this.rating = rating;
}

// App State
const products = [
  new Product(
    1,
    "iPhone 14 Pro Max",
    1700,
    5,
    2,
    "Mobile Phones",
    20,
    [
      { review: "Apple meh :(", rating: 1 },
      { review: "I'm an apple fanboy", rating: 5 },
    ],
    "Apple iPhone",
    "https://fdn2.gsmarena.com/vv/bigpic/apple-iphone-14-pro-max-.jpg"
  ),
  new Product(
    2,
    "iPhone 14 Pro",
    1400,
    10,
    2,
    "Mobile Phones",
    20,
    [],
    "Apple iPhone",
    "https://fdn2.gsmarena.com/vv/bigpic/apple-iphone-14-pro.jpg"
  ),
  new Product(
    3,
    "Samsung Galaxy Z Fold4",
    1799,
    5,
    2,
    "Mobile Phones",
    20,
    [],
    "Samsung Galaxy Z Fold4",
    "https://fdn2.gsmarena.com/vv/bigpic/samsung-galaxy-z-fold4-5g.jpg"
  ),
  new Product(
    4,
    "Samsung Galaxy Z Flip4",
    1500,
    10,
    2,
    "Mobile Phones",
    20,
    [],
    "Samsung Galaxy Z Flip4",
    "https://fdn2.gsmarena.com/vv/bigpic/samsung-galaxy-z-flip4-5g.jpg"
  ),
  new Product(
    5,
    "Samsung Galaxy S22 Ultra 5G",
    1700,
    10,
    1,
    "Mobile Phones",
    20,
    [],
    "Samsung Galaxy S22 Ultra 5G",
    "https://fdn2.gsmarena.com/vv/bigpic/samsung-galaxy-s22-ultra-5g.jpg"
  ),
  new Product(
    6,
    "Samsung Galaxy S22+ 5G",
    1350,
    15,
    2,
    "Mobile Phones",
    20,
    [],
    "Samsung Galaxy S22+ 5G",
    "https://fdn2.gsmarena.com/vv/bigpic/samsung-galaxy-s22-plus-5g.jpg"
  ),
  new Product(
    7,
    "Google Pixel 7",
    936,
    25,
    3,
    "Mobile Phones",
    20,
    [],
    "Google Pixel 7",
    "https://m.media-amazon.com/images/I/7173p0j-mbL._AC_SX522_.jpg"
  ),
  new Product(
    8,
    "Dell XPS 8930",
    1502,
    8,
    1,
    "Computers",
    20,
    [],
    "Dell XPS 8930",
    "https://m.media-amazon.com/images/I/31qXWztXi9L._SL500_.jpg"
  ),
  new Product(
    9,
    "Microsoft Surface Studio 2",
    2800,
    12,
    2,
    "Computers",
    50,
    [],
    "Microsoft Surface Studio 2",
    "https://m.media-amazon.com/images/I/41AYAAocl9L._SL500_.jpg"
  ),
  new Product(
    10,
    "IdeaCentre AIO 3",
    740,
    9,
    2,
    "Computers",
    50,
    [],
    "IdeaCentre AIO 3",
    "https://p1-ofp.static.pub/medias/23389938226_IdeaCentreAIO327ALC6_202211071155261668286253112.png"
  ),
  new Product(
    11,
    "Corsair One Pro i200",
    5692,
    5,
    1,
    "Computers",
    50,
    [],
    "Corsair One Pro i200",
    "https://m.media-amazon.com/images/I/31s2FXCuZKL._SL500_.jpg"
  ),
  new Product(
    12,
    "HP Envy All-In-One",
    3584,
    7,
    1,
    "Computers",
    50,
    [],
    "HP Envy All-In-One",
    "https://m.media-amazon.com/images/I/41PSoXubaiL._SL500_.jpg"
  ),
  new Product(
    13,
    "2021 Apple iMac",
    2300,
    10,
    2,
    "Computers",
    50,
    [],
    "2021 Apple iMac",
    "https://store.storeimages.cdn-apple.com/4982/as-images.apple.com/is/refurb-imac-24-blue-2021?wid=1144&hei=1144&fmt=jpeg&qlt=90&.v=1639423360000"
  ),
  new Product(
    14,
    "Dell OptiPlex 390",
    208,
    4,
    2,
    "Computers",
    50,
    [],
    "Dell OptiPlex 390",
    "https://i5.walmartimages.com/asr/1aee9558-1261-4ec7-a648-5e42972e4bd1.c3cdd3f6e4a0e3cf39e7d680133aa396.jpeg"
  ),
  new Product(
    15,
    "HP Compaq Elite 8200",
    189,
    1,
    1,
    "Computers",
    50,
    [],
    "HP Compaq Elite 8200",
    "https://i5.walmartimages.com/asr/8ad91f13-ca8e-430a-b768-aba0d37fa16e_1.4284e9b39f2b15799db8b964b1c57d0b.jpeg"
  ),
];
let cart = [];
let currencyMultiplier = 1;
let selectedProductId;
let selectedCategory = "All";

// Elements
const productIdInput = document.getElementById("idInput");
const qtyInput = document.getElementById("qtyInput");
const productDetailsBtn = document.getElementById("detailsBtn");
const cartList = document.getElementById("cart--list");
const addItemBtn = document.getElementById("addItem");
const removeItemBtn = document.getElementById("removeItem");
const currencySelector = document.getElementById("currency-selector");
const summary = document.getElementById("summary");
const categoryAll = document.getElementById("category-all");
const categoryMobile = document.getElementById("category-mobile-phones");
const categoryComputers = document.getElementById("category-computers");

// Event Listeners Initialization
addItemBtn.addEventListener("click", handleAddProduct);
removeItemBtn.addEventListener("click", handleRemoveProduct);
currencySelector.addEventListener("change", handleChangeCurrency);
categoryAll.addEventListener("click", (e) => changeCategory("All", e));
categoryMobile.addEventListener("click", (e) =>
  changeCategory("Mobile Phones", e)
);
categoryComputers.addEventListener("click", (e) =>
  changeCategory("Computers", e)
);

// App Functions
function displayProducts() {
  let filteredProducts;

  if (selectedCategory === "All") {
    filteredProducts = products;
  } else {
    filteredProducts = products.filter(
      (product) => product.category === selectedCategory
    );
  }

  const productItems = filteredProducts.map((product) => {
    const { _id, name, price, stock, max, category, image } = product;

    const row = document.createElement("div");
    row.classList.add("row");

    row.innerHTML = `
            <div class="product">
              <img
                class="product--img"
                src="${image}"
                alt="${name}"
              />
              <div class="product--details">
                <h3 class="product--name">${name}</h3>
                <p class="product--detail">
                  <span class="product--detail-label">ID:</span> ${_id}
                </p>
                <p class="product--detail">
                  <span class="product--detail-label">Price:</span> ${renderPrice(
                    price
                  )}
                </p>
                <p class="product--detail">
                  <span class="product--detail-label">Stock:</span> ${stock}
                </p>
                <p class="product--detail">
                  <span class="product--detail-label">Max:</span> ${max}
                </p>
                <p class="product--detail">
                  <span class="product--detail-label">Category:</span>
                  ${category}
                </p>
              </div>
              <div class="product-actions">
                <div class="product-actions-group">
                  <button class="button" onclick="handleClickDetails(${_id})">View Details</button>
                </div>
              </div>
            </div>
          `;
    return row;
  });

  document.getElementById("product-list-col").innerHTML = "";

  document.getElementById("product-list-col").append(...productItems);
}

function handleClickDetails(productId) {
  selectedProductId = productId;

  const {
    _id,
    name,
    price,
    stock,
    max,
    category,
    shippingCost,
    reviews,
    description,
    image,
  } = products.find((product) => product._id === productId);

  document.getElementById("popup").classList.add("show");
  document.getElementById("mask").classList.add("show");
  document.getElementById("popup-product--img").src = image;
  document.getElementById("popup--product-name").innerText = name;
  document.getElementById("popup--product-id").innerText = _id;
  document.getElementById("popup--product-price").innerText =
    renderPrice(price);
  document.getElementById("popup--product-stock").innerText = stock;
  document.getElementById("popup--product-max").innerText = max;
  document.getElementById("popup--product-category").innerText = category;
  document.getElementById("popup--product-shippingCost").innerText =
    renderPrice(shippingCost);
  document.getElementById("popup--product-description").innerText = description;

  displayReviews(reviews);
}

function closeDetails() {
  document.getElementById("popup").classList.remove("show");
  document.getElementById("mask").classList.remove("show");
  selectedProductId = null;
}

// ADD PRODUCT
function handleAddProduct() {
  const productIdEl = document.getElementById("productId");
  const qtyEl = document.getElementById("productQty");

  const productId = parseInt(productIdEl.value);
  const qty = parseInt(qtyEl.value);

  if (!productId || !qty) {
    alert("Proper product ID and quantity are required.");
    return;
  }

  const product = products.find((product) => product._id === productId);
  const existingCartItem = cart.find((item) => item.productId === productId);

  if (!product) {
    alert("Product with entered product ID does not exist.");
    return;
  }

  if (!checkStock(product, existingCartItem, qty)) {
    return;
  }

  if (existingCartItem) {
    existingCartItem.quantity += qty;
  } else {
    cart.push(
      new CartItem(productId, product.price, qty, product.shippingCost)
    );
  }

  product.stock = product.stock - qty;
  productIdEl.value = "";
  qtyEl.value = "";

  displayCart();
  displayProducts();
}

// REMOVE PRODUCT
function handleRemoveProduct() {
  const productIdEl = document.getElementById("productId");
  const qtyEl = document.getElementById("productQty");

  const productId = parseInt(productIdEl.value);
  const qty = parseInt(qtyEl.value);

  if (!productId || !qty) {
    alert("Proper product ID and quantity are required.");
    return;
  }

  const existingCartItem = cart.find((item) => item.productId === productId);

  if (!existingCartItem) {
    alert("Product does not exist in your cart.");
    return;
  }

  if (existingCartItem.quantity < qty) {
    alert("You do not have enough of the same product in your cart.");
    return;
  }

  const product = products.find((product) => product._id === productId);

  if (existingCartItem.quantity === qty) {
    cart = cart.filter((item) => item.productId !== productId);
  } else {
    existingCartItem.quantity -= qty;
  }

  product.stock += qty;

  productIdEl.value = "";
  qtyEl.value = "";

  displayCart();
  displayProducts();
}

function displayCart() {
  if (!cart.length) {
    cartList.innerHTML = '<div class="cart-no-items">No items in cart</div>';
  } else {
    const cartItems = cart.map(({ productId, price, quantity }) => {
      return `
          <div class="cart--item">
            <div class="product-details">
              <p class="product--detail">
                <span class="product--detail-label">ID:</span> ${productId}
              </p>
              <p class="product--detail">
                <span class="product--detail-label">Price:</span> ${renderPrice(
                  price
                )}
              </p>
              <p class="product--detail">
                <span class="product--detail-label">Quantity:</span> ${quantity}
              </p>
              <p class="product--detail">
                <span class="product--detail-label">Subtotal:</span> <span  id="cart-item-subTotal-${productId}">${renderPrice(
        price * quantity
      )}</span>
              </p>
            </div>
          </div>
      `;
    });

    cartList.innerHTML = cartItems.join("");
  }
  computeTotal();
}

// COMPUTE TOTAL
function computeTotal() {
  let itemsSubTotal = cart.reduce(
    (prevSubTotal, item) => prevSubTotal + item.price * item.quantity,
    0
  );

  let totalShipping = cart.reduce(
    (prevShippingTotal, item) =>
      prevShippingTotal + item.shippingCost * item.quantity,
    0
  );

  const subTotal = itemsSubTotal + totalShipping;
  const tax = subTotal * 0.13;
  const total = subTotal + tax;

  summary.innerHTML = `
    <p class="product--detail">
      <span class="product--detail-label">Items Subtotal:</span> ${itemsSubTotal}
    </p>
    <p class="product--detail">
      <span class="product--detail-label">Estimated Shipping:</span> ${totalShipping}
    </p>
    <br/>
    <p class="product--detail">
      <span class="product--detail-label">Subtotal:</span> ${subTotal}
    </p>
    <p class="product--detail">
      <span class="product--detail-label">Estimated Tax:</span> ${tax}
    </p>
    <p class="product--detail">
      <span class="product--detail-label">Order Total:</span> ${total}
    </p>
  `;
}

function submitReview() {
  const ratingEl = document.getElementById("rating-input");
  const reviewEl = document.getElementById("review-input");

  const rating = parseInt(ratingEl.value);
  const review = reviewEl.value;

  if (isNaN(rating) || rating < 1 || rating > 5 || !review) {
    return alert("Product review is invalid. Please check your input.");
  }

  const selectedProduct = products.find(
    (product) => product._id === selectedProductId
  );

  selectedProduct.reviews.push(new ProductReview(review, rating));

  displayReviews(selectedProduct.reviews);
  ratingEl.value = "";
  reviewEl.value = "";
}

// CHANGE CURRENCY
function handleChangeCurrency() {
  const newCurrency = currencySelector.value;

  switch (newCurrency) {
    case "USD":
      currencyMultiplier = 0.74;
      break;
    case "GBP":
      currencyMultiplier = 0.6;
      break;
    default:
      currencyMultiplier = 1;
      break;
  }

  displayCart();
  displayProducts();
}

// CHANGE CATEGORY
function changeCategory(category, e) {
  document
    .querySelectorAll(".product-category")
    .forEach((btn) => btn.classList.remove("active"));
  console.log(e.target.classList.add("active"));
  selectedCategory = category;
  displayProducts();
}

// UTILITY FUNCTIONS
function checkStock(product, existingCartItem, qty) {
  if (product.stock < qty) {
    alert("There is not enough stock for this product.");
    return false;
  }

  if (
    product.max < qty ||
    (existingCartItem && qty + existingCartItem.quantity > product.max)
  ) {
    alert(`You cannot buy more than ${product.max} of this product.`);
    return false;
  }

  return true;
}

function renderPrice(value) {
  const currency = currencySelector.value;
  return `${(value * currencyMultiplier).toFixed(2)} ${currency}`;
}

function renderReview({ review, rating }) {
  let stars = ``;
  for (let i = 0; i < 5; i++) {
    if (i < rating) {
      stars += "&#9733;";
    } else {
      stars += "&#9734;";
    }
  }

  return `
    <div class="reviews-item">
      <div class="stars">${stars}</div>
      <div class="reviews-review">${review}</div>
    </div>
  `;
}

function displayReviews(reviews) {
  const ratings = reviews.map(({ rating }) => rating);
  const sum = ratings.reduce((a, b) => a + b, 0);
  const avgRating = (sum / ratings.length || 0).toFixed(2);

  const reviewItems = reviews.map((review) => renderReview(review)).join("");

  document.getElementById("reviews-average").innerText = avgRating;
  document.getElementById("reviews-list").innerHTML = reviewItems;
}

document.getElementById("time").innerText = new Date().toDateString();

displayCart();
displayProducts();
